#include <catch2/catch_test_macros.hpp>

#include "factorial.h"

TEST_CASE("Factorial check", "[factorial]")
{
	REQUIRE(factorial(5) == 120) ;
	REQUIRE(factorial(0) == 1) ;
	REQUIRE(factorial(1) == 1) ;
	REQUIRE(factorial(6) == 720) ;
	REQUIRE(factorial(5) == 120) ;
}
