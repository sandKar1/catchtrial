#ifndef FACTORIAL_H
#define FACTORIAL_H

#include <cstdint>

uint64_t factorial(uint8_t num) ;

#endif
