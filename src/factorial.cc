#include "factorial.h"

uint64_t factorial(uint8_t num)
{
		uint64_t result = 1 ;
		for(uint8_t index = num; index > 0; --index)
		{
				result *= index ;
		}

		return result ;
}
